package unique

func Unique(data []int) []int {
	b := 0
	for a := 1; a < len(data); a++ {
		if data[a] != data[b] {
			b++
			data[b] = data[a]
		}
	}

	if len(data) > 0 {
		return data[:(b + 1)]
	}

	// empty slice?
	return data
}

func UniqueMap(data []int) []int {
	s := make(map[int]bool, len(data))
	for _, v := range data {
		s[v] = true
	}
	data = data[:0]

	for k, _ := range s {
		data = append(data, k)
	}

	return data
}
