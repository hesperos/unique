package unique

import (
	"fmt"
	"math/rand"
	"sort"
	"testing"
)

func TestUnique(t *testing.T) {
	data := []int{5, 3, 5, 5, 4, 4, 2}
	expected := []int{2, 3, 4, 5}
	sort.Ints(data)
	u := Unique(data)

	for i, v := range expected {
		if v != u[i] {
			t.Fatalf("%d != %d", u[i], v)
		}
	}
}

func BenchmarkInsertion(b *testing.B) {
	data := rand.Perm(32)
	origData := data
	u := []int{}
	for i := 0; i < b.N; i++ {
		data = origData
		u = Unique(data)
	}
	fmt.Println(u)
}

func BenchmarkMap(b *testing.B) {
	data := rand.Perm(32)
	origData := data
	u := []int{}
	for i := 0; i < b.N; i++ {
		data = origData
		u = UniqueMap(data)
	}

	fmt.Println(u)
}
